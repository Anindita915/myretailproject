**MyRetail RESTful Service for Product Information**
* **About the Application**
* Backend: Springboot with Java
* Unit test cases: JUNIT4 and Mockito
* Data Store: MongoDB
* **System Requirements To Run Application**
* IDE: STS/Eclipse 
* Java JDK and JRE should be available on system
* Data: Mongo (latest version), IDE: Compass
* **Interaction with Service**

  This service allows you to retrieve product and price details by supplying a product ID. The data is aggregated from multiple sources and returned as JSON

* **URL**

  /products/{id}

* **Methods:**
  
  `GET` | `POST` | `PUT`
  
*  **URL Params**

   **Required:**
 
   `id=[integer]`

* **Data Params**

  ```JSON 
  { 
      "productId": 13860428,

		"name": "The Big Lebowski (Blu-ray)",

		"current_price": {

		"value": "30.99",

		"currency_code": "USD"
		
		}
  }

* **Success Response:**
	
	A valid payload with a relevant ID returns the following response

  * **Code:** 200
  *  **Content:** 
    ```JSON
    {
		"productId": 13860428,

		"name": "The Big Lebowski (Blu-ray)",

		"current_price": {

		"value": "30.99",

		"currency_code": "USD"
		
	}
 
* **Error Response:**

  Failing to supply the required information in the payload will return the following:

  * **Code:** 400
    **Content:** RequestBody


* **Notes:**

  Performing a `POST` | `PUT` request essentially updates the product price information that            you supply against the product ID

* **Testing with JUNIT4 and Mockito**
* Unit test covering the business layer implements by mocking data repositories or HTTP responses.
* Tests can be found under src/test/java folder

 