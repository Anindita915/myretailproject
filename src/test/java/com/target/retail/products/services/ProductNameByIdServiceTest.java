package com.target.retail.products.services;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class ProductNameByIdServiceTest {
	
	@Test
	public void TestfilterProductNameFromResponse() throws JsonMappingException, JsonProcessingException {
		
		String json=
				" { \"product\": {" + 
				"    \"item\": {" + 
				"      \"tcin\": \"13860428\"," + 
			
				"      \"product_description\": {" + 
				"        \"title\": \"The Big Lebowski (Blu-ray)\"}" + 
			
				"    }" + 
				"}}";
		ResponseEntity<?> response =new ResponseEntity<>(json,HttpStatus.OK);
		
		ProductNameByIdService service = new ProductNameByIdService();
		String name="The Big Lebowski (Blu-ray)";
		String result= service.filterProductNameFromResponse((ResponseEntity<String>) response);
		
		assertEquals(name,result);
	}

}
