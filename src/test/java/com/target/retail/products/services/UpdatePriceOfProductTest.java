package com.target.retail.products.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;

import com.target.retail.products.model.Price;
import com.target.retail.products.model.Products;
import com.target.retail.products.repository.ProductRepositoryImpl;

public class UpdatePriceOfProductTest {
	
	@Test
	public void TestsaveUpdates() {
		
		ProductRepositoryImpl repository= mock(ProductRepositoryImpl.class);
		long id=12345;
		Price current_price= new Price("12.99","USD");
		Products product = new Products(id,"testproduct", current_price); 
		when(repository.updateCurrentPrice(product)).thenReturn(product);
		
		assertEquals(current_price, product.getCurrent_price());
		
	}

}
