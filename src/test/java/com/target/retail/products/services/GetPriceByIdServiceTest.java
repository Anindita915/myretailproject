package com.target.retail.products.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.target.retail.products.model.Price;
import com.target.retail.products.repository.ProductRepository;

public class GetPriceByIdServiceTest {
	
	@Test
	 public void TestsetPriceFromResponse() throws JsonMappingException, JsonProcessingException {

		Price price= new Price("30.99","USD");
		ProductRepository productRepository = mock(ProductRepository.class);
		
		when(productRepository.getPriceById(123456)).thenReturn(price.toString());
		
		PriceByIdService service = new PriceByIdService(productRepository);
		Price result=service.setPriceFromResponse(123456);
		
		assertEquals(price.getValue(), result.getValue());
		assertEquals(price.getCurrency_code(), result.getCurrency_code());
		
	}

}
