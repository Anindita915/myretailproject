package com.target.retail.products.controllers;



import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.target.retail.products.model.Price;
import com.target.retail.products.model.Products;
import com.target.retail.products.repository.ProductRepositoryCustom;
import com.target.retail.products.repository.ProductRepository;
import com.target.retail.products.services.BuildProductDetailsResponseService;
import com.target.retail.products.services.GetDataFromExternalAPIService;

import com.target.retail.products.services.UpdatePriceOfProduct;

@RestController
@EnableMongoRepositories(basePackageClasses = ProductRepositoryCustom.class)
public class ProductsController {
	

	ProductRepository productRepository;
	BuildProductDetailsResponseService response;
	UpdatePriceOfProduct updatePriceService;
	GetDataFromExternalAPIService externalDataService;
	
	@Autowired
	public ProductsController(ProductRepository productRepository, BuildProductDetailsResponseService response,
			UpdatePriceOfProduct updatePrice, GetDataFromExternalAPIService externalDataService) {
		super();
		this.productRepository = productRepository;
		this.response = response;
		this.updatePriceService = updatePrice;
		this.externalDataService = externalDataService;
	}

	@RequestMapping(path="/products/id", method=RequestMethod.GET)
	public ResponseEntity<?> getProductById(@RequestParam long id) throws JsonMappingException, JsonProcessingException {
		
		try {
			String name= externalDataService.getProductName(id);
			
			Price price= externalDataService.getProductPrice(id);
			
			return new ResponseEntity<>(response.buildResponse(price, name, id), HttpStatus.OK);
		}
		catch(Exception ex) {
			return new ResponseEntity<>("", HttpStatus.BAD_REQUEST);
		}
		
		
		
		
	
	}
	
	@RequestMapping(path="/products/id", method=RequestMethod.POST) 

	public ResponseEntity<?> updatePrice(@Valid @RequestBody Products product,@RequestParam long id)
	{
		try {
			Products updatedProduct=updatePriceService.saveUpdates(product);
			return new ResponseEntity<>(updatedProduct, HttpStatus.OK);
		}
		
		catch(Exception ex) {
			return new ResponseEntity<>(product, HttpStatus.BAD_REQUEST);
		}
	}
	
	@RequestMapping(path="/products/id", method=RequestMethod.PUT) 

	public ResponseEntity<Products> updatePriceByPut(@Valid @RequestBody Products product,@RequestParam long id)
	{
		try {
			Products updatedProduct=updatePriceService.saveUpdates(product);
			return new ResponseEntity<>(updatedProduct, HttpStatus.OK);
		}
		
		catch(Exception ex) {
			return new ResponseEntity<>(product, HttpStatus.BAD_REQUEST);
		}
	}

}
