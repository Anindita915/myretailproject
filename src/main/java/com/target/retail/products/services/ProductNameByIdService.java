package com.target.retail.products.services;


import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.target.retail.products.model.ProductName;

@Service
public class ProductNameByIdService {
	 
	ProductName name;

	
	public String filterProductNameFromResponse(ResponseEntity<String> result) throws JsonMappingException, JsonProcessingException {
		
		 String json =result.getBody();
	        ObjectMapper mapper = new ObjectMapper();
	        JsonNode node = mapper.readTree(json);
	        JsonNode title = node.at("/product/item/product_description/title");

	         name = mapper.treeToValue(title, ProductName.class);
	         
	         return name.getName();
		
	}

}
