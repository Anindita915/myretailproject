package com.target.retail.products.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.target.retail.products.model.Products;
import com.target.retail.products.repository.ProductRepositoryImpl;

@Service
public class UpdatePriceOfProduct {



	public ProductRepositoryImpl productRepositoryImpl;
	
	@Autowired
	public UpdatePriceOfProduct(ProductRepositoryImpl productRepositoryImpl) {
		super();
		this.productRepositoryImpl = productRepositoryImpl;
	}
	
	public Products saveUpdates(Products product){
		return productRepositoryImpl.updateCurrentPrice(product);
	}
}
