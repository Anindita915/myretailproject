package com.target.retail.products.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import com.target.retail.products.model.Price;
import com.target.retail.products.model.Products;

@Component
public class BuildProductDetailsResponseService {

	@Autowired 
	private Products product;

	public BuildProductDetailsResponseService(Products product) {
		super();
		this.product = product;
	}
	
	public Products buildResponse(Price current_price, String name, long id) throws JsonMappingException, JsonProcessingException {
		
	
		product.setCurrent_price(current_price);
		product.setName(name);
		product.setProductId(id);
		
		return product;
	}
}
