package com.target.retail.products.services;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.target.retail.products.model.Price;

@Service
public class GetDataFromExternalAPIService {
	
	
	ProductNameByIdService getProductName;
	
	
	PriceByIdService getProductPrice;
	

	
	@Autowired
	public GetDataFromExternalAPIService(ProductNameByIdService getProductName, PriceByIdService getProductPrice) {
		super();
		this.getProductName = getProductName;
		this.getProductPrice = getProductPrice;
		
	}
	
	public String getProductName(long id) {
		
    	final String uri = "https://redsky.target.com/v2/pdp/tcin/"+id +"?excludes=taxonomy,price,promotion,bulk_ship,rating_and_review_reviews,rating_and_review_statistics,question_answer_statistics";
    
    	RestTemplate restTemplate = new RestTemplate();
    
    	HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
         
        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);

        try {
			return getProductName.filterProductNameFromResponse(result);
		} catch (JsonMappingException e) {
			
			e.printStackTrace();
			return "";
		} catch (JsonProcessingException e) {
			
			e.printStackTrace();
			return "";
		}
        
       
  }

 
    public Price getProductPrice(@RequestParam long id) throws JsonMappingException, JsonProcessingException {
    	
    	//Code to call the actual API to fetch price. Currently retrieving from data store"
    	/*final String uri="https://price. myRetail.com/product/{productId}/price";
   	
    	RestTemplate restTemplate = new RestTemplate();
    	    	HttpHeaders headers = new HttpHeaders();    	
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        

        //String price= productRepository.getPriceById(id);*/
        
    	return getProductPrice.setPriceFromResponse(id);
    			
    	
    	
    
    	
    }


	
	

}
