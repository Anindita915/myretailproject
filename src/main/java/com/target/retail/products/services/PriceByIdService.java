package com.target.retail.products.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.target.retail.products.model.Price;
import com.target.retail.products.repository.ProductRepository;

@Service
public class PriceByIdService {

	
	ProductRepository productRepository;
	Price price;
	@Autowired
	public PriceByIdService(ProductRepository productRepository) {
		super();
		this.productRepository = productRepository;
	
	}
	
	public Price setPriceFromResponse(long productId) throws JsonMappingException, JsonProcessingException {
		
		String current_price=productRepository.getPriceById(productId);
		ObjectMapper mapper = new ObjectMapper();
		  JsonNode node = mapper.readTree(current_price);
	        JsonNode cprice = node.at("/current_price");
	         price = mapper.treeToValue(cprice, Price.class);
	        
		//Price obj = mapper.readValue(current_price, Price.class);
		return price;
	}


}
