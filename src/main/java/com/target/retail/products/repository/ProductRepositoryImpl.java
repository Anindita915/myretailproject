package com.target.retail.products.repository;

import static org.springframework.data.mongodb.core.query.Criteria.where;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;


import com.target.retail.products.model.Products;


public class ProductRepositoryImpl implements ProductRepositoryCustom{

	
	private MongoTemplate mongoTemplate;
    //private ProductRepository repository;
	@Autowired
	public ProductRepositoryImpl(MongoTemplate mongoTemplate) {
		super();
		this.mongoTemplate = mongoTemplate;
	
	}
	

	@Override
	public Products updateCurrentPrice(Products product) {
		Query query = new Query();
				query.addCriteria(where("productId").is(product.getProductId()));
		Update update = new Update();
		update.set("current_price", product.getCurrent_price());
		mongoTemplate.update(Products.class).matching(query).apply(update).first();
		
		return (Products) mongoTemplate.find(query, Products.class);
		
	}



}
