package com.target.retail.products.repository;



import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;


import com.target.retail.products.model.Products;
import com.target.retail.products.model.ProductsDAO;

@Repository
public interface ProductRepository extends MongoRepository<ProductsDAO, String>, ProductRepositoryCustom {
	
	Products findByProductId(final long productId);
	
	@Query(value="{ 'productId' : ?0 }",fields="{ 'current_price':1,'_id':0}")
	String getPriceById(final long productId);
	
	

}
