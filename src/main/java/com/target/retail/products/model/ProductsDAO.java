package com.target.retail.products.model;

import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection="products")

public class ProductsDAO {


	Long productId;
	String name; 
	
	
	Price currentprice;
	public ProductsDAO() {}
	
	public ProductsDAO(Long productId, String name, Price current_price) {
		super();
		this.productId = productId;
		this.name = name;
		this.currentprice = current_price;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Price getCurrent_price() {
		return currentprice;
	}

	public void setCurrent_price(Price current_price) {
		
		this.currentprice = current_price;
	}
	
	
	
}
