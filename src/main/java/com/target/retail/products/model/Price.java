package com.target.retail.products.model;

import org.springframework.stereotype.Component;

@Component
public class Price {
	

	String value;
	String currency_code;
	
	public Price() {}
	public Price(String value, String currency_code) {
		super();
	
		this.value = value;
		this.currency_code = currency_code;
	}

	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getCurrency_code() {
		return currency_code;
	}
	public void setCurrency_code(String currency_code) {
		this.currency_code = currency_code;
	}
	

	@Override
	public String toString() {
		return "{ \"current_price\":{"+"\"value\":\""+ value+ "\",\"currency_code\":\""+ currency_code+"\"}}";
		//return "Price [value=" + value + ", currency_code=" + currency_code + "]";
	}

}
