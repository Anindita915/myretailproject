package com.target.retail.products.model;

import org.springframework.stereotype.Component;


@Component
public class Products {

	@Override
	public String toString() {
		return "Products [productId=" + productId + ", name=" + name + ", current_price=" + current_price + "]";
	}

	Long productId;
	String name; 
	
	
	Price current_price;
	public Products() {}
	
	public Products(Long productId, String name, Price current_price) {
		super();
		this.productId = productId;
		this.name = name;
		this.current_price = current_price;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Price getCurrent_price() {
		return current_price;
	}

	public void setCurrent_price(Price current_price) {
		
		this.current_price = current_price;
	}
	
	
	
}
