package com.target.retail.products.model;

import org.springframework.stereotype.Component;

@Component
public class ProductName {
	
	String Name;



	public ProductName() {
		super();
		
	}

	public ProductName(String name) {
		super();
		Name = name;
	}
	public String getName() {
		return Name;
	}

	@Override
	public String toString() {
		return "ProductName [Name=" + Name + "]";
	}
	
	

}
